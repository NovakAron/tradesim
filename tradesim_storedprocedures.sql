DROP PROCEDURE IF EXISTS determineTraderMoneyBalance;
DELIMITER $$
CREATE PROCEDURE determineTraderMoneyBalance(IN traderUid INT)
 BEGIN 

SELECT SUM(a * p) * -1 AS money_balance
FROM
  (SELECT amount AS a,
          price AS p
   FROM
     ( SELECT id, amount, t.sid, IF (t.amount > 0,
                                     sell_price,
                                     buy_price) AS price
      FROM transaction t
      LEFT JOIN stock_quote q ON t.sid=q.sid
      WHERE
        t.uid=traderUid AND
        q.timestamp < t.timestamp
      ORDER BY id,
               q.timestamp DESC ) a
   GROUP BY id) c;

END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS traderTopList;
DELIMITER $$
CREATE PROCEDURE traderTopList()
 BEGIN 

SELECT uid,
       SUM(a * p) * -1 AS money_balance
FROM
  (SELECT amount AS a,
          price AS p,
          uid
   FROM
     (SELECT id, uid, amount, t.sid, IF (t.amount > 0,
                                         sell_price,
                                         buy_price) AS price
      FROM transaction t
      LEFT JOIN stock_quote q ON t.sid=q.sid
      WHERE
        q.timestamp < t.timestamp
      ORDER BY id,
               q.timestamp DESC) a
   GROUP BY id) c
GROUP BY uid
ORDER BY money_balance DESC ;

END$$
DELIMITER ;
