SET foreign_key_checks = 0;
-- -----------------------------------------------------
-- Table `TradeSim`.`stock`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `TradeSim`.`stock` ;

CREATE  TABLE IF NOT EXISTS `TradeSim`.`stock` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `ticker` VARCHAR(15) NOT NULL ,
  `company` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
COMMENT = 'Metadata of stocks';


-- -----------------------------------------------------
-- Table `TradeSim`.`stock_quote`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `TradeSim`.`stock_quote` ;

CREATE  TABLE IF NOT EXISTS `TradeSim`.`stock_quote` (
  `timestamp` TIMESTAMP NOT NULL ,
  `sid` INT NULL ,
  `buy_amount` INT NULL ,
  `buy_price` DECIMAL(10,3) NULL ,
  `sell_amount` INT NULL ,
  `sell_price` DECIMAL(10,3) NULL ,
  PRIMARY KEY (`timestamp`, `sid`),
  FOREIGN KEY (sid) REFERENCES stock(id)
  ON DELETE CASCADE
)
ENGINE = InnoDB
COMMENT = 'List of current offers to buy/share for the specific shares';


-- -----------------------------------------------------
-- Table `TradeSim`.`transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `TradeSim`.`transaction` ;

CREATE  TABLE IF NOT EXISTS `TradeSim`.`transaction` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `uid` INT NOT NULL ,
  `amount` INT NOT NULL ,
  `timestamp` TIMESTAMP NOT NULL ,
  `sid` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  FOREIGN KEY (sid) REFERENCES stock(id)
  ON DELETE CASCADE
)
ENGINE = InnoDB;

SOURCE tradesim_storedprocedures.sql;
