/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim;

import bme.tmit.tradesim.stockexchanges.BudapestStockExchange;
import bme.tmit.tradesim.stockexchanges.StockExchange;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

/**
 * Collects stock quotes for the further simulation from various stock exchanges
 */
public class RecordBook extends TradeSimMode {

    private Map<String, Integer> idCache = new HashMap<>();
    private Timestamp timeOfScrape = null;
    private int samplesCollected = 0;

    @Override
    public void run() {

        List<StockExchange> stockExchanges = new ArrayList<>();
        stockExchanges.add(new BudapestStockExchange());

        while (true) {
            try {
                Iterator stockExchangesIterator = stockExchanges.iterator();

                while (stockExchangesIterator.hasNext()) {
                    StockExchange stockExchange = (StockExchange) stockExchangesIterator.next();

                    if (!stockExchange.isTradingTime(new Timestamp(new java.util.Date().getTime()))) {
                        continue;
                    }

                    List<String> tickerList = stockExchange.getTickerList();
                    Iterator tickerListIterator = tickerList.iterator();

                    while (tickerListIterator.hasNext()) {
                        String ticker = (String) tickerListIterator.next();
                        if (!stockExists(ticker)) {

                            Class exchangeClass = stockExchange.getClass();
                            Class partypes[] = new Class[1];
                            partypes[0] = String.class;
                            String companyName = "";
                            try {
                                Method fetchName = exchangeClass.getMethod("getCompanyName", partypes);
                                companyName = (String) fetchName.invoke(ticker);
                            }
                            finally {
                                addStock(ticker, companyName);
                            }


                        }
                        addQuote(ticker, stockExchange.getBuyQuote(ticker), stockExchange.getSellQuote(ticker));
                        samplesCollected++;
                    }
                }

                // Sampling in every 30 seconds.
                Thread.sleep(30000);
            } catch (java.lang.InterruptedException e) {
                // Normal
                break;
            } catch (Exception e) {
                if (TradeSim.DEBUG_MODE) {
                    e.printStackTrace();
                }
                System.err.println("Error while collecting the quotes.");
                break;
            }
        }
    }

    /**
     * Records a new stock to the database
     */
    private void addStock(String ticker, String company) throws SQLException {
        PreparedStatement pr = conn.prepareStatement("INSERT INTO stock (ticker, company) VALUES (?, ?)");
        pr.setString(1, ticker);
        pr.setString(2, company);
        pr.executeUpdate();
    }

    /**
     * Records a quote to the database
     */
    private void addQuote(String ticker, BookEntry buy, BookEntry sell) throws SQLException {
        PreparedStatement pr = conn.prepareStatement("INSERT INTO stock_quote VALUES (?, ?, ?, ?, ?, ?)");
        pr.setTimestamp(1, timeOfScrape);
        pr.setInt(2, getStockID(ticker));
        pr.setInt(3, buy.getAmount());
        pr.setFloat(4, buy.getPrice());
        pr.setInt(5, sell.getAmount());
        pr.setFloat(6, sell.getPrice());
        pr.executeUpdate();
    }

    /**
     * Queries if a given ticker already exists in the database or not.
     */
    private boolean stockExists(String ticker) throws SQLException {
        PreparedStatement pr = conn.prepareStatement("SELECT 1 FROM stock WHERE ticker = ?");
        pr.setString(1, ticker);
        ResultSet rs = pr.executeQuery();
        if (!rs.next()) {
            return false;
        }
        return true;
    }

    /**
     * Converts ticker to numerical ID (of a stock)
     */
    private int getStockID(String ticker) throws SQLException {
        if (!idCache.containsKey(ticker)) {
            PreparedStatement pr = conn.prepareStatement("SELECT id FROM stock WHERE ticker = ?");
            pr.setString(1, ticker);
            ResultSet rs = pr.executeQuery();
            rs.next();
            idCache.put(ticker, rs.getInt("id"));
        }
        return idCache.get(ticker);
    }

    /**
     * Returns the number of samples collected at the current run (to the controller)
     */
    @Override
    String getStatus() {
        if (samplesCollected < 2) {
            return samplesCollected + " sample was collected.";
        } else {
            return samplesCollected + " samples were collected and processed.";
        }
    }
}
