/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Represents a stock, makes connection between the stock's ticker and its internal ID
 */
public class Stock {

    private int id = 0;

    public Stock(int id) {
        this.id = id;
    }

    public Stock(String ticker) {
        try {
            Connection conn = TradeSimMode.getDB();
            PreparedStatement pr = conn.prepareStatement("SELECT id FROM stock WHERE ticker = ?");
            pr.setString(1, ticker);
            ResultSet rs = pr.executeQuery();
            rs.next();
            this.id = rs.getInt("id");

        } catch (SQLException e) {
            if (TradeSim.DEBUG_MODE) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }
}
