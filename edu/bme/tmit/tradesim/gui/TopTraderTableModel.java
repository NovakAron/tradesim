/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim.gui;

import bme.tmit.tradesim.DisplayResult;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * Provides a model for the overview table in the result window [JTable]
 * Also it handles the export to CSV, as the logic of constructing
 * the CSV is quite similar to constructing a table.
 */
public class TopTraderTableModel extends AbstractTableModel {

    private DisplayResult result;
    private List<HashMap<String, String>> cachedTopResult = null;

    public TopTraderTableModel(DisplayResult result) {
        this.result = result;
    }

    /**
     * Gets
     * @param index
     * @return
     */
    @Override
    public String getColumnName(int index) {
        switch (index) {
            case 0:
                return "Befektető azonosítója";
            case 1:
                return "Elért profit [Ft]";
            case 2:
                return "Elért profit [%]";
        }
        return "";
    }

    @Override
    public int getRowCount() {
        return result.getNumberOfTraders();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (cachedTopResult == null) {
            cachedTopResult = result.getTradersResult();
        }
        HashMap<String, String> entry = cachedTopResult.get(rowIndex);
        if (columnIndex == 0) {
            return entry.get("uid");
        } else if (columnIndex == 1) {
            return entry.get("balance");
        } else {
            HashMap<String, String> topPerformer = cachedTopResult.get(0);
            if (Float.parseFloat(entry.get("balance")) < 0) {
                return "N/A";
            }
            return (Float.parseFloat(entry.get("balance"))
                    / Float.parseFloat((topPerformer.get("balance")))) * 100.0;
        }
    }

    public String exportToFile() throws IOException {
        String now = new SimpleDateFormat("yyyy-MMM-dd").format(Calendar.getInstance().getTime());
        String filename = "topList" + now + ".txt";
        FileWriter fstream = new FileWriter(filename);
        BufferedWriter out = new BufferedWriter(fstream);
        int cols = this.getColumnCount();
        int rows = this.getRowCount();
        for (int i = 0; i < cols; i++) {
            out.write("\"" + this.getColumnName(i) + "\";");
        }
        out.write("\n");
        for (int j = 0; j < rows; j++) {
            for (int i = 0; i < cols; i++) {
                out.write("\"" + this.getValueAt(j, i) + "\";");
            }
            out.write("\n");
        }

        out.close();
        return filename;

    }
}
