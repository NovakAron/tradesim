/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim;

import bme.tmit.tradesim.gui.DisplayResultWindow;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * The 'display' mode of the simulator. Provides data for the swing window.
 */
public class DisplayResult extends TradeSimMode implements Runnable {

    @Override
    public void run() {
        DisplayResultWindow window = new DisplayResultWindow(this);
        window.setVisible(true);
    }

    /**
     * Returns the result of a single trader
     */
    public float getTraderMoneyBalance(int traderID) {
        try {
            PreparedStatement pr;
            pr = conn.prepareStatement("CALL determineTraderMoneyBalance(?)");
            pr.setInt(1, traderID);
            ResultSet rs = pr.executeQuery();
            rs.next();
            return rs.getFloat(1);
        } catch (SQLException e) {
            return (float) 0.0;
        }
    }

    /**
     * Returns the number of traders who ever made transactions.
     */
    public int getNumberOfTraders() {
        try {
            PreparedStatement pr;
            pr = conn.prepareStatement("SELECT COUNT(DISTINCT uid) FROM transaction");
            ResultSet rs = pr.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            return 0;
        }
    }

    /**
     * Get the full ordered list of the result of the traders, descending.
     */
    public List<HashMap<String, String>> getTradersResult() {
        List<HashMap<String, String>> result = new ArrayList<>();
        try {

            PreparedStatement pr;
            pr = conn.prepareStatement("CALL traderTopList()");
            ResultSet rs = pr.executeQuery();
            while (rs.next()) {
                HashMap<String, String> entry = new HashMap<>();
                entry.put("uid", new Integer(rs.getInt(1)).toString());
                entry.put("balance", new Float(rs.getFloat(2)).toString());
                result.add(entry);
            }

        } catch (SQLException e) {
        }
        return result;
    }

    /**
     * We don't have any specific status message for the GUI mode.
     */
    @Override
    String getStatus() {
        return "OK";
    }
}
