/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

abstract public class TradeSimMode implements Runnable {

    static protected Connection conn = null;
    private String url = "jdbc:mysql://127.0.0.1:3306/";
    private String dbName = "TradeSim";
    private String driver = "com.mysql.jdbc.Driver";
    private String userName = "root";
    private String password = "aa";

    /**
     * All modes of the simulator requires the database connection, that's why
     * it's initialized here.
     */
    public TradeSimMode() {

        if (conn == null) {
            try {
                Class.forName(driver).newInstance();
                conn = DriverManager.getConnection(url + dbName, userName, password);

                if (conn == null) {
                    throw new SQLException();
                }
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
                if (TradeSim.DEBUG_MODE) {
                    e.printStackTrace();
                }
                System.err.println("Failed to connect to the database");
            }
        }
    }

    abstract String getStatus();

    static Connection getDB() throws SQLException {
        if (conn == null) {
            throw new SQLException("The connection is not yet initialized. A simulation mode has to be started before using the database.");
        }
        return conn;
    }
}
