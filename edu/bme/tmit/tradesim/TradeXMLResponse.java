/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim;

import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author aaron
 */
public class TradeXMLResponse {

    public static String response(String error) {
        return TradeXMLResponse.forgeXML("0", "error", error);
    }

    public static String response(float price) {
        return TradeXMLResponse.forgeXML("1", "price", new Float(price).toString());
    }

    private static String forgeXML(String status, String additionalElem, String additionalElemText) {
        try {
            org.w3c.dom.Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            org.w3c.dom.Element root = doc.createElement("tradeResult");
            doc.appendChild(root);
            org.w3c.dom.Element statusElement = doc.createElement("status");
            org.w3c.dom.Element additionalElement = doc.createElement(additionalElem);
            statusElement.setTextContent(status);
            additionalElement.setTextContent(additionalElemText);
            root.appendChild(statusElement);
            root.appendChild(additionalElement);
            if (TradeSim.DEBUG_MODE) {
                System.err.println(TradeXMLResponse.forgeDoc2Text(doc));
            }
            return TradeXMLResponse.forgeDoc2Text(doc);

        } catch (ParserConfigurationException e) {
            if (TradeSim.DEBUG_MODE) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public static String forgeDoc2Text(org.w3c.dom.Document doc) {
        try {
            TransformerFactory transfac = TransformerFactory.newInstance();
            Transformer trans = transfac.newTransformer();
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(doc);
            trans.transform(source, result);
            return sw.toString();
        } catch (TransformerException e) {
            if (TradeSim.DEBUG_MODE) {
                e.printStackTrace();
            }
            return "";
        }
    }
}
