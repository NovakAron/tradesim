/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim;

import com.martiansoftware.jsap.*;
import java.sql.Timestamp;

/**
 * Controls the loading and initializing the subcomponents of the application. -
 * Recording mode - Simulation mode - Result mode
 */
public class TradeSim {

    public static final boolean DEBUG_MODE = Boolean.getBoolean("debug");

    public static void main(String[] args) {
        Thread runningPart;

        try {

            // Processing command-line arguments
            SimpleJSAP jsap = new SimpleJSAP(
                    "TradeSim",
                    "A trading-robot simulator tool. It can operates in three modes."
                    + " - Recording mode: scrapes real-time share quotes from Budapest Stock Exchange (BÉT)"
                    + " - Simulation mode: uses the pre-recorded quotes and provides a web API to make transactions for external parties (robots)"
                    + " - Result mode: shows the summary of the performance of the ",
                    new Parameter[]{
                        new FlaggedOption("mode", JSAP.STRING_PARSER, "record", JSAP.REQUIRED, 'm', JSAP.NO_LONGFLAG, "Operation mode."),
                        new FlaggedOption("time", JSAP.INTEGER_PARSER, "60", JSAP.NOT_REQUIRED, 'n', JSAP.NO_LONGFLAG, "The number of seconds to operate (not effective in result mode)."),
                        new FlaggedOption("start", JSAP.INTEGER_PARSER, "1336906063", JSAP.NOT_REQUIRED, 't', JSAP.NO_LONGFLAG, "Timestamp when the simulation should be started (virtual time, the book of the offers will be served according to this."),});
            JSAPResult config = jsap.parse(args);
            if (jsap.messagePrinted()) {
                System.exit(1);
            }

            TradeSimMode subSys = null;
            switch (config.getString("mode")) {
                case "record":
                    subSys = new RecordBook();
                    break;
                case "sim":
                    Timestamp simStart = new Timestamp(new Integer(config.getInt("start")).longValue() * 1000);
                    subSys = new SimulateTrading(simStart);
                    break;
                case "result":
                    subSys = new DisplayResult();
                    break;
            }


            if (subSys == null) {
                throw new InterruptedException("Initialization error");
            }

            runningPart = new Thread(subSys);
            runningPart.start();

            if (!subSys.getClass().getSimpleName().equals("DisplayResult")) {
                Thread.sleep(config.getInt("time") * 1000);
                System.out.println(subSys.getClass().getSimpleName() + ": " + subSys.getStatus());
                System.exit(0);
            }

        } catch (JSAPException | InterruptedException e) {
            if (TradeSim.DEBUG_MODE) {
                e.printStackTrace();
            }
            System.err.println("An error happened while initializing or running the component of the simulator.");
        }

    }
}
