/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim.stockexchanges;

import bme.tmit.tradesim.BookEntry;
import java.sql.Timestamp;
import java.util.*;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.xml.sax.InputSource;

/**
 * Collects data from the Budapest Stock Exchange via scraping an AJAX callback
 * path of the bet.hu site.
 */
public class BudapestStockExchange implements StockExchange {

    private static final String DATA_URL = "http://bet.hu/ajaxdata?pid=AJI1004_1104";
    private static final int MIN_SCRAPE_INTERVAL = 120;
    private Map<String, String> xPathes = new HashMap<>();
    private HashMap<String, BookEntry> buyQuoteCache = new HashMap<>();
    private HashMap<String, BookEntry> sellQuoteCache = new HashMap<>();
    private Timestamp lastScrape = null;

    /**
     * Initializes the xPath-selector map with the proper XPath expresssions to scrape bet.hu data
     */
    public BudapestStockExchange() {
        this.xPathes.put("countStocks", "count(//table/tbody[1]/tr/td[1])");
        this.xPathes.put("stockName", "(//table/tbody[1]/tr/td[1])[:elemnum]");
        this.xPathes.put("stockSellAmount", "(//table/tbody[1]/tr/td[9])[:elemnum]");
        this.xPathes.put("stockSellPrice", "(//table/tbody[1]/tr/td[8])[:elemnum]");
        this.xPathes.put("stockBuyAmount", "(//table/tbody[1]/tr/td[7])[:elemnum]");
        this.xPathes.put("stockBuyPrice", "(//table/tbody[1]/tr/td[6])[:elemnum]");
        // Try to mimics a non-robot software
        System.setProperty("http.agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3");
    }

    /**
     * @todo This function is supposed to determine if the stock exchange is open or closed according to:
     * http://bet.hu/topmenu/piacok_termekek/kereskedes/kereskedesi_ido
     *
     * @param time
     * @return
     */
    @Override
    public boolean isTradingTime(Timestamp time) {
        return true;
    }

    /**
     * Fetches all the tickers of the A-category stocks at BÉT
     * @return
     */
    @Override
    public List<String> getTickerList() {
        this.scrape();
        List<String> tickers = new ArrayList<>();
        Iterator tickerIterator = sellQuoteCache.keySet().iterator();
        while (tickerIterator.hasNext()) {
            tickers.add((String) tickerIterator.next());
        }
        return tickers;
    }

    /**
     *
     * @param ticker
     * @return
     */
    @Override
    public BookEntry getSellQuote(String ticker) {
        this.scrape();
        return sellQuoteCache.get(ticker);
    }

    @Override
    public BookEntry getBuyQuote(String ticker) {
        this.scrape();
        return buyQuoteCache.get(ticker);
    }

    @Override
    public BookEntry getSellQuote(String ticker, Timestamp time) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public BookEntry getBuyQuote(String ticker, Timestamp time) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Does the actual data scraping from the bet.hu AJAX-callback URL.
     */
    private void scrape() {
        boolean doScrape = false;

        if (lastScrape == null) {
            doScrape = true;
        } else {
            java.util.Date now = new java.util.Date();
            if ((now.getTime() - lastScrape.getTime()) > (BudapestStockExchange.MIN_SCRAPE_INTERVAL * 1000)) {
                doScrape = true;
            }
        }

        if (doScrape) {
            try {
                XPathFactory factory = XPathFactory.newInstance();
                XPath xPath = factory.newXPath();
                InputSource quoteXMLPage = new InputSource(BudapestStockExchange.DATA_URL);
                int numShares = Integer.parseInt(xPath.compile(getBETXPath("countStocks")).evaluate(quoteXMLPage));

                // the xPath [] operator starts the indexing from 1, that's why we do so here.
                for (int i = 1; i <= numShares; i++) {
                    String shareName = xPath.compile(getBETXPath("stockName", i)).evaluate(quoteXMLPage).trim();
                    BookEntry buy = new BookEntry();
                    BookEntry sell = new BookEntry();
                    buy.setAmount(getQuotePart(true, true, i, quoteXMLPage, xPath));
                    buy.setPrice(getQuotePart(true, false, i, quoteXMLPage, xPath));
                    sell.setAmount(getQuotePart(false, true, i, quoteXMLPage, xPath));
                    sell.setPrice(getQuotePart(false, false, i, quoteXMLPage, xPath));
                    buyQuoteCache.put(shareName, buy);
                    sellQuoteCache.put(shareName, sell);
                    java.util.Date date = new java.util.Date();
                    lastScrape = new Timestamp(date.getTime());
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * Gets a buy or a sell quote from the actually downloaded XML.
     */
    private int getQuotePart(boolean buy, boolean amount, int elem, InputSource xml, XPath xpath) throws XPathExpressionException {
        String purpose;
        if (buy) {
            purpose = (amount ? "stockBuyAmount" : "stockBuyPrice");
        } else {
            purpose = (amount ? "stockSellAmount" : "stockSellPrice");
        }
        try {
            String xpathHit = xpath.compile(getBETXPath(purpose, elem)).evaluate(xml).replaceAll("[^0-9]", "");
            return Integer.parseInt(xpathHit);
        } catch (XPathExpressionException | NumberFormatException e) {
            return 0;
        }
    }

    private String getBETXPath(String purpose, int elem) {
        Integer elemnum = new Integer(elem);
        return getBETXPath(purpose).replace(":elemnum", elemnum.toString());
    }

    private String getBETXPath(String purpose) {
        return this.xPathes.get(purpose);
    }
}
