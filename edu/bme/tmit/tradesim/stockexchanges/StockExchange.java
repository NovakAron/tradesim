/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim.stockexchanges;

import bme.tmit.tradesim.BookEntry;
import java.sql.Timestamp;
import java.util.List;

public interface StockExchange {

    public boolean isTradingTime(Timestamp time);

    public List<String> getTickerList();

    public BookEntry getSellQuote(String ticker);

    public BookEntry getBuyQuote(String ticker);

    public BookEntry getSellQuote(String ticker, Timestamp time);

    public BookEntry getBuyQuote(String ticker, Timestamp time);
}
