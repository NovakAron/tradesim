/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim;

public class TradeException extends Exception {

    public TradeException(String msg) {
        super(msg);
    }

    public String asXML() {
        return TradeXMLResponse.response(this.getMessage());
    }
}
