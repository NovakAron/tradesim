/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Represents an offer in the stock's quotes, a price-amount pair.
 */
public class BookEntry {

    private int amount = 0;
    private float price = 0;

    /**
     * @return the amount
     */
    public int getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(float price) throws Exception {
        if (price < 0) {
            throw new Exception("Invalid price for the stock");
        }
        this.price = price;
    }

    /**
     * Formats the quote part as XML
     *
     * @return Valid XML snippet what contains the quote part's data
     */
    public String asXML() {
        try {
            org.w3c.dom.Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            org.w3c.dom.Element root = doc.createElement("bookEntry");
            doc.appendChild(root);
            org.w3c.dom.Element amountElement = doc.createElement("amount");
            org.w3c.dom.Element priceElement = doc.createElement("price");
            amountElement.setTextContent(new Integer(this.getAmount()).toString());
            priceElement.setTextContent(new Float(this.getPrice()).toString());
            root.appendChild(amountElement);
            root.appendChild(priceElement);
            return TradeXMLResponse.forgeDoc2Text(doc);
        } catch (ParserConfigurationException e) {
            if (TradeSim.DEBUG_MODE) {
                e.printStackTrace();
            }
        }
        return "";
    }
}
