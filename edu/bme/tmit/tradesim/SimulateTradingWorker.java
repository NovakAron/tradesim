/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim;

import java.io.*;
import java.net.Socket;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.xml.sax.InputSource;

public class SimulateTradingWorker implements Runnable, TradeEngine {

    private Socket clientSocket;
    private int traderID;
    private Connection conn;
    PrintWriter out;

    SimulateTradingWorker(Socket client) {

        this.clientSocket = client;
        try {
            out = new PrintWriter(clientSocket.getOutputStream(), true);
        } catch (IOException e) {
            Logger.getLogger(SimulateTradingWorker.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void run() {
        try {
            this.conn = TradeSimMode.getDB();

            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            String inputLine, fullInput = "";

            while ((inputLine = in.readLine()) != null && clientSocket.isConnected()
                    && !inputLine.isEmpty()) {
                fullInput += inputLine;
            }

            XPathFactory factory = XPathFactory.newInstance();
            XPath xPath = factory.newXPath();
            InputSource request = new InputSource(new StringReader(fullInput));

            // Required parts of the request
            this.traderID = Integer.parseInt(xPath.compile("//traderID").evaluate(request).trim());
            request = new InputSource(new StringReader(fullInput));
            Stock stock = new Stock(xPath.compile("//share").evaluate(request).trim());

            try {

                // Placing an order
                request = new InputSource(new StringReader(fullInput));
                int amount = Integer.parseInt(xPath.compile("//amount").evaluate(request).trim());
                try {
                    float price = makeContract(stock, amount);
                    out.print(TradeXMLResponse.response(price));
                } catch (TradeException e) {
                    out.print(e.asXML());
                }


            } catch (XPathExpressionException | NumberFormatException e) {

                // Fetching the quotes for the given stock
                BookEntry sell = getBook(stock, false);
                BookEntry buy = getBook(stock, true);
                out.print("<stock><sell>" + sell.asXML() + "</sell><buy>"
                        + buy.asXML() + "</buy></stock>");

            }
            out.flush();
        } catch (SQLException | IOException | XPathExpressionException e) {
            out.print("Internal error happened while processing the request.");
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {
                Logger.getLogger(SimulateTradingWorker.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    @Override
    public float makeContract(Stock stock, int amount) throws TradeException {
        try {
            // If the user wants to sell, the "buy" part of the book is interesting
            // and for buy, the "sell"
            BookEntry entry = (amount > 0) ? getBook(stock, false) : getBook(stock, true);
            if (entry.getAmount() < Math.abs(amount)) {
                throw new TradeException("Amount is insufficient in the stock's book");
            }


            conn.setAutoCommit(false);
            PreparedStatement pr;
            pr = conn.prepareStatement(""
                    + " INSERT INTO"
                    + "   transaction (uid, amount, timestamp, sid)"
                    + " VALUES"
                    + "   (?, ?, ?, ?)");
            pr.setInt(1, traderID);
            pr.setInt(2, amount);

            pr.setTimestamp(3, SimulateTrading.getSimTime());
            pr.setInt(4, stock.getId());
            pr.executeUpdate();
            pr = conn.prepareStatement(""
                    + " SELECT"
                    + "   IF(SUM(amount) IS NULL, 0, SUM(amount))"
                    + " FROM"
                    + "   transaction"
                    + " WHERE"
                    + "   uid = ? AND"
                    + "   sid=?");
            pr.setInt(1, traderID);
            pr.setInt(2, stock.getId());
            ResultSet rs = pr.executeQuery();
            rs.next();
            if (rs.getInt(1) < 0) {
                throw new TradeException("The transaction would result in negative"
                        + "number of shares for the trader.");
            }
            conn.commit();

            return entry.getPrice();
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (SQLException e1) {
            } finally {
                throw new TradeException("Unknown error in executing the transaction.");
            }
        }
    }

    @Override
    public BookEntry getBook(Stock stock, boolean buy) {
        BookEntry entry = new BookEntry();
        try {

            PreparedStatement pr;
            if (buy) {
                pr = conn.prepareStatement(""
                        + " SELECT"
                        + "   buy_amount,"
                        + "   buy_price"
                        + " FROM"
                        + "   stock_quote"
                        + " WHERE"
                        + "   sid = ? AND"
                        + "   timestamp < ?"
                        + " ORDER BY"
                        + "   timestamp"
                        + " DESC LIMIT 1");
            } else {
                pr = conn.prepareStatement(""
                        + " SELECT"
                        + "   sell_amount,"
                        + "   sell_price"
                        + " FROM"
                        + "   stock_quote"
                        + " WHERE"
                        + "   sid = ? AND"
                        + "   timestamp < ?"
                        + " ORDER BY"
                        + "   timestamp"
                        + " DESC LIMIT 1");
            }
            pr.setInt(1, stock.getId());
            pr.setTimestamp(2, SimulateTrading.getSimTime());
            ResultSet rs = pr.executeQuery();
            rs.next();
            entry.setAmount(rs.getInt(1));
            entry.setPrice(rs.getFloat(2));

        } catch (Exception ex) {
            if (TradeSim.DEBUG_MODE) {
                ex.printStackTrace();
            }
        }
        return entry;
    }
}
