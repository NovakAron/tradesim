/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim;

public interface TradeEngine {

    /**
     * Sells or buys the given stock
     *
     * @param ticker
     * @param amount
     * @return The price what was used in the transaction.
     */
    public float makeContract(Stock stock, int amount) throws TradeException;

    /**
     * Retrieves the current book entry for the given stock
     *
     * @param ticker
     * @param buy
     * @return
     */
    public BookEntry getBook(Stock stock, boolean buy);
}
