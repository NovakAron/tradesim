/*
 * TradeSim application
 *
 * BMEVIHIJV37 course HomeWork
 * Aron Novak <aron@novaak.net>
 * E0GOTW
 */
package bme.tmit.tradesim;

import java.io.IOException;
import java.net.ServerSocket;
import java.sql.Timestamp;

/**
 * Handles the multithreaded-ness of client (robot) serving, maintains
 * the SimulateTradingWorkes instances.
 */
public class SimulateTrading extends TradeSimMode {

    private int numerOfRequestsHandled = 0;
    private static Timestamp startedSimTime;
    private static Timestamp startedRealTime = new Timestamp(new java.util.Date().getTime());

    public SimulateTrading(Timestamp simTime) {
        super();
        startedSimTime = simTime;
        java.util.Date date = new java.util.Date();
        startedRealTime = new Timestamp(date.getTime());

    }

    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(1864);
            while (true) {
                SimulateTradingWorker worker;
                try {
                    worker = new SimulateTradingWorker(serverSocket.accept());
                    Thread t = new Thread(worker);
                    t.start();
                    numerOfRequestsHandled++;
                } catch (IOException e) {
                    if (TradeSim.DEBUG_MODE) {
                        e.printStackTrace();
                    }
                    System.out.println("Failed to accept request.");
                }
            }
        } catch (IOException e) {
            if (TradeSim.DEBUG_MODE) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Returns the number of requests served at the current run (to the controller)
     */
    @Override
    String getStatus() {
        if (numerOfRequestsHandled < 2) {
            return numerOfRequestsHandled + " request has been served.";
        } else {
            return numerOfRequestsHandled + " requests have been served.";
        }
    }

    /**
     * Calculates the internal time of the simulation
     */
    public static Timestamp getSimTime() {
        java.util.Date now = new java.util.Date();
        return new Timestamp((now.getTime() - SimulateTrading.startedRealTime.getTime()) + startedSimTime.getTime());
    }
}
